package com.donnatto.mongoapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoapirestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoapirestApplication.class, args);
	}

}
