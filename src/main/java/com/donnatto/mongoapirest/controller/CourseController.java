package com.donnatto.mongoapirest.controller;

import com.donnatto.mongoapirest.model.Course;
import com.donnatto.mongoapirest.service.CourseService;
import com.donnatto.mongoapirest.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Constants.COURSES_URL)
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping
    public ResponseEntity<List<Course>> getAllCourses() {
        List<Course> allCourses = courseService.findAll();
        return (allCourses != null && !allCourses.isEmpty()) ?
                ResponseEntity.ok(allCourses) :
                new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Course> getCourse(@PathVariable String id) {
        Course course = courseService.findById(id);
        return course != null ? ResponseEntity.ok(course) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<Course> saveCourse(@RequestBody Course course) {
        return ResponseEntity.ok(courseService.save(course));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateCourse(@RequestBody Course course, @PathVariable String id) {
        Course foundCourse = courseService.findById(id);
        if (foundCourse == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        courseService.update(course, id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCourse(@PathVariable String id) {
        Course foundCourse = courseService.findById(id);
        if (foundCourse == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        courseService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
