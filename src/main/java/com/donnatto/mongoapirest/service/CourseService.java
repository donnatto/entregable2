package com.donnatto.mongoapirest.service;

import com.donnatto.mongoapirest.model.Course;

import java.util.List;

public interface CourseService {

    List<Course> findAll();
    Course findById(String id);
    Course save(Course course);
    void update(Course course, String id);
    void delete(String id);
}
