package com.donnatto.mongoapirest.service.impl;

import com.donnatto.mongoapirest.model.Course;
import com.donnatto.mongoapirest.repository.CourseRepository;
import com.donnatto.mongoapirest.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course findById(String id) {
        return courseRepository.findById(id).orElse(null);
    }

    @Override
    public Course save(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public void update(Course course, String id) {
        course.setCourseId(id);
        courseRepository.save(course);
    }

    @Override
    public void delete(String id) {
        courseRepository.deleteById(id);
    }

}
