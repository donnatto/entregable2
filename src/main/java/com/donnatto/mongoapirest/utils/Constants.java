package com.donnatto.mongoapirest.utils;

public class Constants {

    public static final String BASE_URL = "/mongoapi/v1";
    public static final String COURSES_URL = BASE_URL + "/courses";
}
